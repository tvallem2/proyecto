var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

//Uso de la librería cors
var cors = require('cors');
app.use(cors());

//variable para el encriptado de las password
var sha512 = require('js-sha512');

var path = require('path');

//variable para la conexión a MLAB (a la db de Ángel)
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

//variable para la conexión al mongo que tenemos en el contenedor local
//si este proyecto va a ir en un contenedor, no puedo usar localhost, debo usar el nombre del contenedor que tiene el mongo
var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";

//variable para la conexión al contenedor postgreSQL
//la URL del servidor es el nombre del contenedor
//además requiere el nombre de la base de datos
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";
//Línea para cuando se llame desde docker
//var urlUsuarios = "postgres://docker:docker@postgresdb:5433/bdseguridad";
var clientePostgre = new pg.Client(urlUsuarios);
clientePostgre.connect();

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//GET para la db en MLAB
// app.get('/movimientos', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//     clienteMlab.get('', function(err, resM, body) {
//       if(err)
//       {
//         console.log(body);
//       }
//       else
//       {
//         res.send(body);
//       }
//     });
// });

// app.get('/clientes', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//     clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
//       if(err)
//       {
//         console.log(body);
//       }
//       else
//       {
//         res.send(body);
//       }
//     });
// });

// app.post('/movimientos', function(req, res) {
//   /* Crear movimiento en MLab */
//
// });

//GET para la db del contenedor Mongo
app.get('/clientesMongo', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("Connected successfully to server");
        var col = db.collection('clientes');
        col.find({}).limit(10).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    });
});

//GET para la db del contenedor Mongo
//Consulta para obtener el listado de cuentas de un cliente
//Se saca el identificador de la cuenta, su alias y el balance
app.get('/cuentasUsuarioOK/:idCliente', function(req, res){
  mongoClient.connect(url,function(err, db){
    if (err)
    {
      console.log(err);
    }
    else {
      console.log("cuentasUsuario: Connected successfully to server");
      var col = db.collection('clientes');
      col.find({"idCliente":parseInt(req.params.idCliente)},{"cuentas.idCuenta":1,"cuentas.descCuenta":1,"cuentas.balance":1}).toArray(function(err, docs) {
      //col.find({"idCliente":parseInt(req.params.idCliente)}).toArray(function(err, docs) {
        res.send(docs[0].cuentas);
        console.log("Consulta cuenta usuario: " + req.params.idCliente);
      })
    }
  });
});

//GET para la db del contenedor Mongo
//Consulta para obtener el listado de cuentas de un cliente
//Se saca el identificador de la cuenta, su alias y el balance
//Revisar -- NO FUNCIONA
app.get('/cuentasUsuario/:idCliente', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("cuentasUsuario: Connected successfully to server");
        var col = db.collection('clientes');
        col.find({"idCliente":parseInt(req.params.idCliente)},{"cuentas.idCuenta":1,"cuentas.descCuenta":1,"cuentas.balance":1}).toArray(function(err, docs) {

          var listaCuentas = docs.cuentas;

          console.log("Consulta cuenta usuario: " + req.params.idCliente);
          console.log(docs);
          console.log(listaCuentas);

          res.send(listaCuentas);
        });
        db.close();
      }
    });
});

//GET para la db del contenedor Mongo
// Obtención de movimientos
app.get('/movimientosMongo', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        col.find({}).limit(10).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    });
});

//GET para la db del contenedor Mongo
// Obtención movimientos de la cuenta
app.get('/movimientosMongo/:idCuenta', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        col.find({"idCuenta":parseInt(req.params.idCuenta)}).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    });
});

//POST para la db del contenedor Mongo (Ejemplo)
// app.post ('/movimientosEjemplo', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//     mongoClient.connect(url,function(err, db) {
//       var col = db.collection('prueba');
//       console.log("Traying to insert to server");
//       //Comento las dos primeras formas de inserción ya que voy a hacerlo por el body
//       // db.collection('movimientos').insertOne({a:1}, function(err, r){
//       //   console.log(r.insertedCount + ' registros insertados');
//       // });
//       //
//       //
//       // //Insert multiple documents
//       // db.collection('movimientos').insertMany([{a:2}, {a:3}], function(err, r){
//       //   console.log(r.insertedCount + ' registros insertados');
//       // });
//
//       db.collection('movimientos').insertOne(req.body, function(err, r){
//         console.log(r.insertedCount + ' registros insertados body');
//       });
//
//       db.close();
//       res.send("ok");
//     })
// });

//POST para la db del contenedor Mongo
// Inserción de un movimiento del cliente
app.post('/movimientoMongo',function(req, res){
  mongoClient.connect(url,function(err,db){
    if(err) {
      console.log(err);
    }
    else {
      //Insert mediante body
      console.log("Connected successfully to server");

      db.collection('movimientos').insertOne(req.body, function(err, r){
        console.log(r.insertedCount + ' registros insertados body');
      });

      res.send("OK");
      db.close();
    }

  });
});


//Servicio para la validación del login contra la base de datos postgreSQL
app.post ('/login', function(req, res) {
  // Crear cliente postgreSQL
  clientePostgre.connect();
  //console.log('Estoy conectado');
  //Hacer consulta
  //El tercer parámetro

  const query = clientePostgre.query('SELECT COUNT(*) FROM users WHERE login=$1 AND password=$2;', [req.body.login, sha512(req.body.password)], (err, result) => {
  if (err){
    console.log(err);
    res.send(err);
  }
  else {
    //Esto sólo vale para mostrar el primer registro
    //NO está contralando si no se devuelve nada o bien si se devuelve más de un registro (que habría que controlarlo)
    if (result.rows[0].count>= 1)
    {
      res.send("Login correcto");
    }
    else {
      //console.log (result.rows[0]);
      //res.send(result.rows[0]);
      res.send("Login incorrecto");
    }
  }
});
});

//Servicio para el registro de nuevos usuarios de la aplicaión
//Registro en la base de datos postgreSQL
app.post ('/registro', function(req, res) {
  // Crear cliente postgreSQL
  clientePostgre.connect();


  const queryId = clientePostgre.query('SELECT max(id) FROM users;', (err, result) => {
    if (err){
      console.log(err);
    }
    else {
      console.log(result.rows);
      newId = result.rows[0];
      console.log("NewId: "+ newId);
    }
  });

  //const newId = 4;

  // const query =  clientePostgre.query('insert into users (id,login,password,name,surname,email) values (newId,$1,$2,$3,$4,$5);', [req.body.login, sha512(req.body.password), req.body.name, req.body.surname, req.body.email], (err, result) => {
  //   if (err){
  //     console.log(err);
  //     res.send(err);
  //   }
  //   else {
  //     console.log("New user created");
  //     res.send("New user created");
  //   }
  // });
});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
});
